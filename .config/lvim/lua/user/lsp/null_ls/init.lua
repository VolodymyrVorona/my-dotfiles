local null_ls = require("null-ls")
local user_utils = require("user.utils")

local null_ls_code_actions = null_ls.builtins.code_actions
local diagnostics = null_ls.builtins.diagnostics
local formatting = null_ls.builtins.formatting

local code_actions = require("lvim.lsp.null-ls.code_actions")
local linters = require("lvim.lsp.null-ls.linters")
local formatters = require("lvim.lsp.null-ls.formatters")

local config_file_names_eslint = require("user.lsp.config-file-names").eslint
-- local config_file_names_prettier = require("user.lsp.config-file-names").prettier

local filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact" }

code_actions.setup({
	null_ls_code_actions.eslint_d.with({
		filetypes = filetypes,
		condition = function(nls_utils)
			return nls_utils.root_has_file(config_file_names_eslint) or user_utils.is_in_package_json("eslint")
		end,
		prefer_local = "node_modules/.bin",
	}),
})

linters.setup({
	diagnostics.eslint_d.with({
		filetypes = filetypes,
		condition = function(nls_utils)
			return nls_utils.root_has_file(config_file_names_eslint) or user_utils.is_in_package_json("eslint")
		end,
		prefer_local = "node_modules/.bin",
	}),
})

formatters.setup({
	formatting.prettierd,
	-- formatting.prettier.with({
	--   filetypes = { "javascript", "javascriptreact", "typescript", "typescriptreact" },
	--   condition = function(nls_utils)
	--     return nls_utils.root_has_file(config_file_names_prettier) or user_utils.is_in_package_json("prettier")
	--   end,
	--   prefer_local = "node_modules/.bin",
	-- }),
})
